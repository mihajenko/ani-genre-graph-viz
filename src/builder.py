import os
from collections import Counter
from itertools import permutations

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from tinydb import Query
from tinydb import TinyDB

import constants

# init DB
_db = TinyDB(constants.DB_PATH)

# make relevant DB query
query = Query()
results = _db.search(
    (query.type == 'tv') &
    ((query.status == 'finished') | (query.status == 'airing'))
)

n_anime = len(results)
print('queried {0} TV anime'.format(n_anime))

# discover genres
genres = {genre for anime in results for genre in anime['genres'] if genre}

print('got {0} anime genres:'.format(len(genres)))
print(', '.join(genres))

# build graph
g = nx.Graph()
# add genres as nodes
g.add_nodes_from(genres)

# counter for different 2-combos of genres
two_genres = Counter()

# add edges with weights
for anime in results:
    # unique-ify genres
    _genres = {genre for genre in anime['genres'] if genre}
    for genre in _genres:
        # increase genre size (=number of anime with this genre)
        try:  # noinspection PyUnresolvedReferences
            g.node[genre]['size'] += 1
        except KeyError:  # noinspection PyUnresolvedReferences
            g.node[genre]['size'] = 1

        # add score
        score = anime['score']
        if score is not None:
            try:  # noinspection PyUnresolvedReferences
                g.node[genre]['scores'].append(score)
            except KeyError:  # noinspection PyUnresolvedReferences
                g.node[genre]['scores'] = [score]
    # add edges (=each permutation of different genres)
    for u, v in permutations([genre for genre in _genres if genre], r=2):
        two_genres[tuple(sorted((u, v)))] += 1
        g.add_edge(u, v)

# calculate median scores for each genre
for genre in genres:
    # noinspection PyUnresolvedReferences
    median_score = np.median(g.node[genre]['scores'])
    # noinspection PyUnresolvedReferences,PyTypeChecker
    g.node[genre]['median'] = float(median_score)
    # noinspection PyUnresolvedReferences
    del g.node[genre]['scores']

# weigh the edges
for uv, count in two_genres.items():
    u, v = uv
    g[u][v]['weight'] = count / n_anime

# write to GraphML format
nx.write_gexf(g, os.path.join(constants.RESOURCES_ROOT, 'export.gexf'))
