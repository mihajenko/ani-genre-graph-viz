import os

# root folder of this project
PROJECT_ROOT = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..')
RESOURCES_ROOT = os.path.join(PROJECT_ROOT, 'resources')
DB_PATH = os.path.join(RESOURCES_ROOT, 'db.json')
