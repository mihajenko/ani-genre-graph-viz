import logging
import os
import random
from functools import partial
from multiprocessing import Pool
from multiprocessing import Semaphore

import requests
from tinydb import Query
from tinydb import TinyDB
from bs4 import BeautifulSoup
from bs4 import NavigableString
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import TimeoutError

import util
import constants


# errors
class CrawlError(Exception):
    def __init__(self, message='unknown error'):
        self.message = message


# setup logging
logger = logging.getLogger('crawler')
hdlr = logging.FileHandler('crawler.log')
formatter = logging.Formatter('%(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.WARNING)

# init DB
_db = TinyDB(constants.DB_PATH)

# default folder path for saving HTML
_html_root = os.path.join(constants.RESOURCES_ROOT, 'html')

# load needed request headers and proxies
_headers = util.load_headers_list()
_proxies = util.load_proxies_list()


def parse_html(aid, content):
    # parse HTML
    soup = BeautifulSoup(content, 'lxml')

    def find_navigable_string(_el):
        # search for a NavigableString
        for i in range(5):
            sibling = _el.next_sibling
            if isinstance(sibling, NavigableString):
                ret_sibling = sibling
                break
        else:
            raise LookupError('No NavigableString instance found')

        return ret_sibling

    def convert_navigable_string(_ns):
        return str(_ns).strip().rstrip().lower()

    # extract info from HTML
    body = soup.body
    try:
        title = body.find('a').text
    except (AttributeError, TypeError):
        save_dic = {}
    else:
        save_dic = {'aid': aid, 'title': title}
        for span in body.find_all('span'):
            span_start = span.text.strip().startswith

            # get genres
            if span_start('Genres'):
                ns = find_navigable_string(span)
                s = convert_navigable_string(ns)
                save_dic['genres'] = s.split(', ')

            # get status
            if span_start('Status'):
                ns = find_navigable_string(span)
                s = convert_navigable_string(ns)
                if s.startswith('finished'):
                    save_dic['status'] = 'finished'
                elif s.startswith('currently'):
                    save_dic['status'] = 'airing'
                elif s.startswith('not'):
                    save_dic['status'] = 'future'
                else:
                    save_dic['status'] = 'undefined'

            # get type
            if span_start('Type'):
                ns = find_navigable_string(span)
                save_dic['type'] = convert_navigable_string(ns)

            # get anime user score & numerus
            if span_start('Score'):
                ns = find_navigable_string(span)
                s = convert_navigable_string(ns)
                try:
                    save_dic['score'] = float(s)
                except ValueError:
                    save_dic['score'] = None
                small = ns.next_sibling
                s = small.text.strip().rstrip().lower()  # <small>
                save_dic['score_n'] = int(s.split(' ')[2].replace(',', ''))

            # get members who added the anime
            if span_start('Members'):
                ns = find_navigable_string(span)
                s = convert_navigable_string(ns)
                save_dic['members'] = int(s.replace(',', ''))

    return save_dic


_url_base = 'https://myanimelist.net/includes/ajax.inc.php?t=64&id='


def fetch_url(url, headers, proxy):
    return requests.get(url, headers=headers, proxies=proxy)


def exec_request(url):
    # threads for timing out requests if they take too long
    thread_pool = ThreadPoolExecutor(max_workers=1)

    # choose headers and proxies
    headers = random.choice(_headers)
    proxy = random.choice(_proxies)
    fut = thread_pool.submit(partial(fetch_url, url, headers, proxy))

    # noinspection PyBroadException
    try:
        resp = fut.result(timeout=10)
    except TimeoutError:
        raise CrawlError('Timeout')
    except requests.exceptions.ProxyError:
        raise CrawlError('Proxy error')
    except requests.exceptions.RequestException:
        raise CrawlError('Request error')
    except Exception as e:
        print(e)
        raise CrawlError('Unknown exception')
    else:  # response received
        # check status code
        status = resp.status_code
        if status == 407:
            proxy_line = proxy['http']
            raise CrawlError(f'Bad proxy auth ({proxy_line})')
        elif resp.status_code != 200:  # request OK
            raise CrawlError('Bad status code')

        try:  # decode the response
            html_content = resp.text
        except (UnicodeDecodeError, UnicodeEncodeError):
            raise CrawlError('Decoding error')

    return html_content


def process_one(aid):
    # create URL
    url = f'{_url_base}{aid}'

    try:  # get content for possible anime ID
        content = exec_request(url)
    except CrawlError as e:
        exc_msg = f'AID {aid: 5}: {e.message}'
        print(exc_msg)
        return aid, exc_msg
    else:
        print(f'AID {aid: 5}: Successfully downloaded', end='')

    # parse content, may not contain it
    parsed = parse_html(aid, content)
    if parsed:
        print(', parsed')
        parsed = None
    else:
        print(', failed to parse')

    return aid, parsed


def crawl_index():
    # init multiprocessing
    n_threads = 8
    multi_pool = Pool(n_threads)
    sem = Semaphore(n_threads)

    # noinspection PyUnusedLocal
    def cb(res):
        sem.release()
        # unpack result
        _aid, data = res
        if data:
            if isinstance(data, dict):
                # save parsed anime info data
                _db.upsert(data, Query().aid == _aid)
            elif isinstance(data, str):
                logger.info(data)

    def ecb(exc):
        print(exc)

    # MAL-specific range of anime IDs to check for download
    for aid in range(30000, 60000):
        # process in parallel
        sem.acquire()
        multi_pool.apply_async(process_one, args=(aid,), callback=cb,
                               error_callback=ecb)


def _main():
    crawl_index()


def _parse_args():
    import argparse

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    index_p = subparsers.add_parser('get-index')
    index_p.add_argument('dest', type=str, default='')


if __name__ == '__main__':
    _main()
