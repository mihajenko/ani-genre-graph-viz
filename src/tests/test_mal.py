import os

import constants
import scrappers.mal as mal


def test_parse_html():
    fn = os.path.join(constants.RESOURCES_ROOT, 'html', 'mal_test.html')
    with open(fn, 'r') as fp:
        content = fp.read()

    parsed = mal.parse_html(996, content)
    print(parsed)
    assert 'title' in parsed
    assert 'genres' in parsed
    assert 'score' in parsed
    assert 'score_n' in parsed
    assert 'members' in parsed
    print(parsed)


if __name__ is '__main__':
    test_parse_html()
