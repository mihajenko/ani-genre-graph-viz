import os
import pickle
import re

import requests
from bs4 import BeautifulSoup


from constants import RESOURCES_ROOT


def _generate_header_list(agents, pickle_path='headers_list.pkl'):
    """
    Generate a list of HTTP headers for use with requests library and save
    them to a pickled list.

    :param agents: User agent strings, list
    :param pickle_path: System path where to save the generated list as a
                        pickle, string
    :return: Generated HTTP headers, list
    """
    # mobile flag
    mobile_flag = re.compile('phone|mobil', flags=re.I)

    header_list = []
    # iterate user agents
    for agent in agents:
        # skip mobile user agents
        if re.search(mobile_flag, agent):
            print('SKIPPING USER-AGENT: {0}'.format(agent))
            continue
        # quant flag
        for q in range(5, 8):
            # Do-Not-Track flag
            for dnt in range(0, 2):
                header_list.append({
                    'User-Agent': agent,
                    'Accept': ('text/html,application/xhtml+xml,'
                               'application/xml;'
                               'q=0.9,*/*;q=0.{0}').format(q),
                    'Accept-Language': 'en-us;q=0.5',
                    'Accept-Encoding': 'gzip, deflate',
                    'DNT': str(dnt),
                    'Cache-control': 'private, max-age=0, no-cache'
                })
    try:
        with open(pickle_path, 'wb') as fp:
            pickle.dump(header_list, fp)
            fp.close()
    except IOError:
        print('HEADERS generated, but unable to save them '
              'to file "{0}"'.format(pickle_path))
    else:
        print('Successfully saved the generated headers file.')

    return header_list


# Load a pickled HTTP request header list from adjacent directory "resources"
_headers_list_path = os.path.join(RESOURCES_ROOT, 'headers_list.pkl')


def load_headers_list(path=_headers_list_path):
    """
    Load pickled list of headers from ``path``. If non-existent, generate it
    from web source.

    :param path: Headers list pickle file path, str
    """
    try:
        with open(path, 'rb') as fp:
            heads = pickle.load(fp)
            fp.close()
    except IOError:
        print('WARNING: Unable to open user agent pickle file, trying to '
              'build it from text file ...')

        # Common User Agent strings, taken from:
        # https://techblog.willshouse.com/2012/01/03/most-common-user-agents/
        # User agent list filename path
        ua_fn = os.path.join(RESOURCES_ROOT, 'user_agent_list.txt')
        try:
            with open(ua_fn) as fptr:
                uas = fptr.readlines()
                if not uas:
                    raise SystemExit('User agent list file empty')
                uas = [ua.rstrip() for ua in uas if ua]
        except IOError:
            print('WARNING: Unable to open user agent string file; '
                  'trying to fetch it from the web ...')
            try:
                ua_url_source = ('https://techblog.willshouse.com/2012/01/03/'
                                 'most-common-user-agents/')
                resp = requests.get(ua_url_source)
            except requests.exceptions.RequestException:
                raise SystemExit('Unable to load user agent list')
            else:
                soup = BeautifulSoup(resp.content, 'html.parser')
                uas = [t.text for t in
                       soup.find_all('td', {'class': 'useragent'})]

        if uas:
            ua_list_string = '\n'.join(uas)
        else:
            raise SystemExit('No user agents')

        try:
            with open(ua_fn, 'w') as fptr:
                fptr.write(ua_list_string)
        except IOError:
            print(
                'Unable to save user agent list text file; trying to generate '
                'and save the pickle file ...')
        finally:
            heads = _generate_header_list(uas, _headers_list_path)

    return heads


_proxies_list_path = os.path.join(RESOURCES_ROOT, 'proxies.txt')


def load_proxies_list(path=_proxies_list_path):
    """
    Load proxies list from ``path``.

    :param path: Proxies list text file path, str
    :return: Proxies dicts, list
    """
    with open(path, 'r') as fp:
        proxies_list = []
        for ln in fp.readlines():
            ln = ln.rstrip()
            if not ln:
                continue
            proxies_list.append({'http': ln, 'https': ln})
    if not proxies_list:
        raise SystemExit('No "proxies" file found')

    return proxies_list
